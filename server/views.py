# functionality
from flask import Blueprint, make_response, request
from base64 import b64encode
import requests
# system
import html_
import datetime

views = Blueprint(__name__, 'views')

PASSWORDS = ('452', 'hagit')
TEMPLATE = html_.TITLE + \
           html_.BODY_STYLE + \
           html_.build_html_button('srcs/black eagles.png', 'get_qr', (960, 166))
REQUEST_QR = {"data": "",
              "config": {"body": "japnese", "eye": "frame0", "eyeBall": "ball18", "erf1": [], "erf2": [], "erf3": [],
                         "brf1": [], "brf2": [], "brf3": [], "bodyColor": "#000000", "bgColor": "#FFFFFF",
                         "eye1Color": "#000000", "eye2Color": "#000000", "eye3Color": "#000000",
                         "eyeBall1Color": "#555555", "eyeBall2Color": "#555555", "eyeBall3Color": "#555555",
                         "gradientColor1": "#000000", "gradientColor2": "#555555", "gradientType": "linear",
                         "gradientOnEyes": True, "logo": "https://i.ibb.co/Wc2QWMS/B-E.png", "logoMode": "clean"},
              "size": 1000, "download": "imageUrl", "file": "png"}


def make_tiny(url):
    req = f'http://tinyurl.com/api-create.php?url={url}'
    res = requests.get(req)
    return res.text


def get_qr_url(text):
    req = REQUEST_QR
    req['data'] = text
    res = requests.post("https://qr-generator.qrcode.studio/qr/custom", json=req)
    return res.json()['imageUrl']


@views.route('/')
def home():
    """:return: index page.
    """
    return TEMPLATE


@views.route('/get_qr')
def get_qr():
    """EX: /get_qr?password=452&box=1&start=12.3.2005_22:00&end=12.12.2030_22:00
    """
    # get parameters from get request
    params = request.args
    box_num = params.get('box')
    start = params.get('start')
    end = params.get('end')
    password = params.get('password')

    # if a parameter missing
    if not (start and end and password and box_num):
        return TEMPLATE + \
               html_.build_form()

    # if the password is incorrect
    if password not in PASSWORDS:
        return TEMPLATE + \
               html_.build_html_alert(f'.הסיסמה שהוזנה איננה נכונה') + \
               html_.build_form()

    # if the box number is not 1 (there is 1 box in the system)
    if box_num not in '001':
        return TEMPLATE + \
               html_.build_html_alert(f'.{box_num} לא נמצאה התיבה ') + \
               html_.build_form()

    # reformat auto html format
    if 'T' in start and 'T' in end:
        start = start.replace('%3A', '.')
        end = end.replace('%3A', '.')
        start_date = datetime.datetime.strptime(start, '%Y-%m-%dT%H:%M')
        end_date = datetime.datetime.strptime(end, '%Y-%m-%dT%H:%M')
        start = start_date.strftime('%d.%m.%Y_%H:%M')
        end = end_date.strftime('%d.%m.%Y_%H:%M')

    qr_content = b64encode(f'{box_num}_{start}_{end}'.encode('utf-8')).decode()  # encode the parameters with base64
    qr_url = get_qr_url(qr_content)  # generate a qr code with the encoded content
    tiny_url = make_tiny(qr_url)

    return TEMPLATE + \
           html_.build_text(f'בר-קוד לסריקה עבור התיבה {box_num}\n'
                            f'\nתקף בין התאריכים\n{end.replace("_", " ")}\t«\t{start.replace("_", " ")}\n\nלשיתוף הבר קוד עם השליח\n<a href="{tiny_url}" style="color:DarkGray;">{tiny_url}</a>\n') + \
           html_.build_html_button(qr_url, qr_url, (960, 960))  # image of the qr code


@views.route('/<string:image_dir>/<string:image_name>.png')
def get_png(image_dir, image_name):
    """:return: binary png image.
    """
    path = f'{image_dir}/{image_name}.png'
    try:
        image = html_.load_binary(path)
    except FileNotFoundError:
        return 'File not found.'

    response = make_response(image)
    response.headers.set('Content-Type', 'image/png')
    response.headers.set('Content-Disposition', 'attachment', filename=path)
    return response


@views.route('/<string:font_dir>/<string:font_name>.ttf')
def get_font(font_dir, font_name):
    """:return: binary font file.
    """
    path = f'{font_dir}/{font_name}.ttf'
    try:
        font = html_.load_binary(path)
    except FileNotFoundError:
        return 'File not found.'

    response = make_response(font)
    response.headers.set('Content-Type', 'font/ttf')
    response.headers.set('Content-Disposition', 'attachment', filename=path)
    return response


@views.route('/favicon.ico')
def get_ico():
    """:return: binary font file.
    """
    try:
        font = html_.load_binary('favicon.ico')
    except FileNotFoundError:
        return 'File not found.'

    response = make_response(font)
    response.headers.set('Content-Type', 'image/x-icon')
    response.headers.set('Content-Disposition', 'attachment', filename='favicon.ico')
    return response
