from views import views
from flask import Flask

LISTENING_PORT = 80


def start_server():
    print(f'HTTP| Listening on http://localhost:{LISTENING_PORT}')
    app = Flask(__name__)
    app.register_blueprint(views, url_prefix='/')
    app.run(host='0.0.0.0', debug=False, port=LISTENING_PORT)


if __name__ == '__main__':
    start_server()
