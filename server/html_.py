# system
import datetime

TITLE = '<title>mailboxes system</title>'
BODY_STYLE = '<body style="background-color:rgb(0, 0, 0);text-align:center">'


def build_form():
    return f"""{build_text('תאריך התחלה  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  תאריך סיום')}
         <form><div>
         <input id="end" type="datetime-local" name="end" min="{(datetime.datetime.now() - datetime.timedelta(1)).strftime('%Y-%m-%dT%H:%M')}" max="2025-01-01T00:00" required>
         <h>{'&nbsp;' * 40}</h>
         <input id="start" type="datetime-local" name="start" min="{(datetime.datetime.now() - datetime.timedelta(1)).strftime('%Y-%m-%dT%H:%M')}" max="2025-01-01T00:00" required>
         {build_text('מספר התיבה')}
         <input type="number" id="box" name="box" min="1" max="100">
         {build_text('סיסמה לאימות')}
         <input type="password" id="pass" name="password" minlength="3" required>
         <span class="validity"></span>
         </div><div><p>&nbsp;</p><p>&nbsp;</p>
         <input type="submit" value="קבל קוד" style="font-size: 40px;white-space: pre-line; background-color: #555555;
         border: 100; color: white; padding: 16px 32px;
         text-decoration: none; margin: 4px 2px;
         cursor: pointer;;font-family:Assistant-ExtraBold, Bitstream Vera Serif, serif;">
         </div></form>"""


def build_text(text, size=50):
    return f'<style> @font-face {{ font-family: "Assistant-ExtraBold"; src: url("srcs/Assistant-ExtraBold.ttf"); }} </style>' \
           f'<p style="color:white; font-size: {size}px;white-space: pre-line;font-family:Assistant-ExtraBold, Bitstream Vera Serif, serif;">{text}</p>'


def build_html_button(img_path, onclick_path, size=(400, 400), invert=False):
    """:return: image tag linked to input path.
    """
    button = ''
    if invert:
      button += '''<style>img {
-webkit-filter: invert(1);
filter: invert(1);
}</style>'''

    button += f'''<a href="/{onclick_path}">
<img src="{img_path}" width="{size[0]}" height="{size[1]}"></a href><p></p>'''

    return button


def build_html_alert(alert):
    """:return: alert tag with input text.
    """
    return f'</a><script>alert("{alert}");</script>'


def load_binary(path):
    """:return: binary file read.
    """
    with open(path, 'rb') as f:
        return f.read()


def load_text(path):
    """:return: textual file read.
    """
    with open(path, 'r+', encoding="utf8") as f:
        return f.read().replace('\n', '')
