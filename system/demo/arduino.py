# functionality
import cv2

# system
import time
import logging

# definitions
logger = logging.getLogger()


class MailboxSystem:
    def __init__(self, board_path=None, door_pin=None, button_pin=None):
        self.window_name = 'mailbox system'
        cv2.namedWindow(self.window_name)

        self.images = {'open': cv2.imread('demo/open.png'),
                       'close': cv2.imread('demo/close.png')
                       }
        # start with closed door
        self.back = 'close'
        cv2.imshow(self.window_name, self.images[self.back])
        cv2.waitKey(1)

    def open_door(self, print_log='door opened'):
        # open the door
        self.back = 'open'
        cv2.imshow(self.window_name, self.images[self.back])
        cv2.waitKey(1)
        logger.info(print_log)

    def close_door(self, print_log='door closed'):
        # close the door
        self.back = 'close'
        cv2.imshow(self.window_name, self.images[self.back])
        cv2.waitKey(1)

        logger.info(print_log)

    def write_screen(self, text, sec=2, print_log=''):
        text += '\n' if text.find('\n') == -1 else ''  # extend to 2 lines
        str1, str2 = text.split('\n')[:2]  # separate the strings

        frame = self.images[self.back]  # set the background
        # clear the screen
        frame = cv2.putText(frame, '-', (45, 225), cv2.FONT_HERSHEY_SIMPLEX, 6, (229, 192, 152), 50)
        # draw the 2 lines
        frame = cv2.putText(frame, str1, (55, 165), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (100, 100, 100), 2)
        frame = cv2.putText(frame, str2, (55, 185), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (100, 100, 100), 2)
        cv2.imshow(self.window_name, frame)
        cv2.waitKey(1)

        if print_log:
            logger.info(print_log)
        time.sleep(sec)  # wait (for letting the user read)

    def wait_click(self, print_log=''):
        while not (cv2.waitKey(1) & 0xFF in (ord('w'), ord('W'), ord('\''))):  # get w click
            pass


if __name__ == '__main__':
    print('Wrong program.')
