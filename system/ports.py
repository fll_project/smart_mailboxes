"""
define the relevant ports and inputs.
"""

# COM5 / COM3 -> usb ports
BOARD_PORT = 'COM5'

# 0 / 1 -> camera input
CAMERA_INPUT = 0

# arduino pins DON'T CHANGE
MOTOR_PIN = 2
BUTTON_PIN = 3
