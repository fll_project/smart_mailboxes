# local
from arduino import MailboxSystem
from qr_code import QrScanner
import ports

# system
import time
import datetime
from base64 import decodebytes
import logging
from sys import argv

# definitions
LOG_FILE = 'doc.log'

logger = logging.getLogger()
scanner = QrScanner(ports.CAMERA_INPUT, presentation=argv[1] == '-f' if len(argv) > 1 else False)
system = MailboxSystem(ports.BOARD_PORT, ports.MOTOR_PIN, ports.BUTTON_PIN)


def show_time():
    t = time.strftime('%H:%M:%S')
    system.write_screen(f'Show a QR-code\n{t}', sec=0)


def is_legal_date(string):
    # special demo code
    if string == '452':
        return 0, f'box {1} opened.'

    try:
        def string_to_datetime(str_date, str_time):
            return datetime.datetime(*(list(int(d) for d in str_date.split('.'))[::-1] +  # date
                                       list(int(d) for d in str_time.split(':'))))  # time

        string = decodebytes(string.encode()).decode()  # decode the input with base64
        data = string.split('_')
        box_num = data[0]

        # get datetime objects of start, end, now
        start_date = string_to_datetime(data[1], data[2])
        end_date = string_to_datetime(data[3], data[4])
        curr_date = datetime.datetime.now()

        # invalid box
        if int(box_num) != 1:
            return -1, f'Box {box_num} not found.'

        # valid datetime
        if start_date <= curr_date <= end_date:
            return int(box_num), f'box {box_num} opened.'

        # invalid datetime
        return -1, 'Date expired'

    # invalid input
    except Exception:
        return -1, 'Illegal code'


def open_door_click(num):
    system.open_door()
    system.write_screen(f'Box {("00" + str(num))[-3:]} opened\nClose at end', sec=0)
    system.wait_click()  # wait for closing
    system.close_door()
    system.write_screen('Bye Bye!', sec=1)


if __name__ == '__main__':
    # set the log file
    logging.basicConfig(filename=LOG_FILE,
                        format='%(asctime)s - %(levelname)s - %(message)s',
                        filemode='a',
                        level=logging.DEBUG)

    # start scanning
    try:
        scanner.start_scanning(lfilter=is_legal_date,
                               pass_prn=open_door_click,
                               fail_prn=system.write_screen,
                               run_prn=show_time)

    # exception while scanning
    except Exception:
        logger.error('scan crashed')

    # end scanning
    else:
        scanner.stop_scanning()
        system.write_screen(f'System off since\n{time.strftime("%d/%m  %H:%M")}', sec=0)
