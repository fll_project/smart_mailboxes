# functionality
import cv2
from pyzbar.pyzbar import decode as find_qr_codes, ZBarSymbol

# system
import time
from datetime import datetime
import numpy as np
import logging
from os import environ
environ['PYGAME_HIDE_SUPPORT_PROMPT'] = 'hide'
from pygame import mixer

# definitions
DEMO_CODE = '452'

logger = logging.getLogger()


class BGRColors:
    LIGHT_GRAY = (220, 220, 220)
    DARK_GRAY = (50, 50, 50)
    RED = (36, 36, 200)
    GREEN = (0, 200, 20)


class QrScanner:
    def __init__(self, cam_input, window_name='scanner', height=1080, width=1920, presentation=False):
        self.height = height
        self.width = width
        self.window_name = window_name
        self.fps = 0
        self.show_fps = True
        self.presentation = presentation

        self.cap = cv2.VideoCapture(cam_input, cv2.CAP_DSHOW)
        self.cap.set(3, width)
        self.cap.set(4, height)

        self.used_codes = set()
        self.invalid_codes = set()
        self.images = {'load': cv2.imread('srcs/load.png'),
                       'pass': cv2.imread('srcs/pass.png'),
                       'overlay': cv2.imread('srcs/overlay.png', cv2.IMREAD_UNCHANGED)
                       }

        mixer.init()  # init sound output
        cv2.namedWindow(window_name, cv2.WND_PROP_FULLSCREEN)  # init full screen window
        self.presentation_mode() if presentation else None  # edit properties if presentation mode
        cv2.imshow(self.window_name, self.images['load'])  # show loading image
        cv2.waitKey(1)

    def presentation_mode(self):
        cv2.setWindowProperty(self.window_name, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
        self.show_fps = False
        self.presentation = True

    def start_scanning(self, lfilter, pass_prn, fail_prn, run_prn, print_log='scan started'):
        # setup
        logger.critical(print_log)
        prev = time.time()
        prev_frame_time = 0

        while True:
            # execute the run_prn once a second
            if (time.time() - prev) > 1:
                run_prn()
                prev = time.time()

            frame = self.show_frame()

            # show the current frame
            if DEMO_CODE in self.used_codes:
                self.used_codes.remove(DEMO_CODE)
                frame = self.show_frame()

            for code in get_codes(frame):
                # check the validation of the code
                validation = lfilter(code[0])
                # run the relevant prn by the validation
                self.output_prn(validation, pass_prn, fail_prn, frame, *code)

            # click actions
            button = chr(cv2.waitKey(1) & 0xFF)
            if button in 'qQ/':  # close the window
                break
            elif button in 'fFכ':
                self.presentation_mode()
            elif button in '123456789' and not self.presentation:
                self.output_prn((int(button), 0), pass_prn, fail_prn, self.images['pass'], '452', [0, 0, 0, 0])

            # calculate how many frames per second
            if self.show_fps:
                prev_frame_time = self.calc_fps(prev_frame_time)

    def stop_scanning(self, print_log='scan stopped'):
        self.cap.release()
        cv2.destroyAllWindows()
        logger.critical(print_log)

    def draw_msg(self, f, msg, rect, color):
        x, y, w, h = rect  # set the values

        # add a rectangle over the rect
        f = cv2.rectangle(f, (x + 2, y + 2), (x + w, y + h), color, 2)

        # add text msg up to the rectangle
        f = cv2.putText(f, msg, (x + 2, y - 8), cv2.FONT_HERSHEY_SIMPLEX, w / 200, color, 2)
        f = cv2.putText(f, msg, (x, y-10), cv2.FONT_HERSHEY_SIMPLEX, w / 200, BGRColors.LIGHT_GRAY, 2)

        # show and return
        cv2.imshow(self.window_name, f)
        return f

    def show_frame(self):
        f = self.cap.read()[1]  # get frame from camera
        f = alpha_merge(self.images['overlay'], f, 349, 760)  # add overlay foreground to frame

        # add clock to frame
        t = time.strftime('%H:%M:%S')  # get time string
        f = cv2.putText(f.copy(), t, (1015, int(self.height / 2.1)), cv2.FONT_HERSHEY_DUPLEX, 1.8, BGRColors.DARK_GRAY, 7)
        f = cv2.putText(f.copy(), t, (1015 - 4, int(self.height / 2.1) - 4), cv2.FONT_HERSHEY_DUPLEX, 1.8, BGRColors.LIGHT_GRAY, 7)

        # add fps to frame
        if self.show_fps:
            f = cv2.putText(f.copy(), f'{int(self.fps)} fps', (20, 50), cv2.FONT_HERSHEY_DUPLEX, 1, BGRColors.DARK_GRAY, 3)
            f = cv2.putText(f.copy(), f'{int(self.fps)} fps', (20 - 2, 50 - 2), cv2.FONT_HERSHEY_DUPLEX, 1, BGRColors.LIGHT_GRAY, 3)

        # show and return
        cv2.imshow(self.window_name, f)
        return f

    def output_prn(self, validation, pass_prn, fail_prn, f, qr_data, rect):
        # valid code
        if validation[0] != -1:
            if qr_data not in self.used_codes:  # check if already scanned
                # face detection - way too slow
                """
                faceCascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
                gray = cv2.cvtColor(f, cv2.COLOR_BGR2GRAY)
                faces = faceCascade.detectMultiScale(gray, scaleFactor=1.05, minNeighbors=15)
                if not len(faces):
                    return
                x, y, w, h = faces[0]
                f = cv2.rectangle(f, (x, y), (x + w, y + h), (0, 255, 0), 3)
                print( x, y, w, h)
                cv2.imshow(self.window_name, f)
                
                # draw no face warning msg
                self.draw_msg(f, f'Show face', rect, BGRColors.GREEN)
                """
                self.used_codes.add(qr_data)  # add to scanned valid used codes
                logger.info(f'valid qr code scanned {{{qr_data}}}')

                # play pass sound
                mixer.music.load('srcs/pass.wav')
                mixer.music.play()

                # save the frame into 'camera' folder
                cv2.imwrite(f'camera/box{validation[0]}_{datetime.now().strftime("%d,%m,%Y_%H,%M")}.jpg', f)

                # open the box
                cv2.imshow(self.window_name, self.images['pass'])
                cv2.waitKey(1)
                pass_prn(validation[0])

            else:
                # draw already scanned warning msg
                self.draw_msg(f, f'Already used', rect, BGRColors.GREEN)

        # invalid code
        else:
            if qr_data not in self.invalid_codes:  # check if already scanned
                self.invalid_codes.add(qr_data)  # add to scanned invalid codes
                logger.error(f'invalid qr code scanned {{{qr_data}}}')

                # play error sound
                mixer.music.load('srcs/fail.wav')
                mixer.music.play()

            # draw invalid message error msg
            self.draw_msg(f, validation[1], rect, BGRColors.RED)

    def calc_fps(self, prev):
        new_frame_time = time.time()
        self.fps = 1 / (new_frame_time - prev)
        return new_frame_time


def get_codes(f):
    """
    splits the qr-codes scan for saving run time.
    :param f: The frame to search code in.
    :return: the codes with their places.
    """
    f = (cv2.cvtColor(f, cv2.COLOR_BGR2GRAY))  # gray
    thresh = cv2.threshold(f, 120, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]

    # system settings
    kernel = np.ones((3, 3), np.uint8)
    thresh = cv2.dilate(thresh, kernel, iterations=1)

    # cut the frame to boxes
    contours = cv2.findContours(thresh, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)[0]
    boxes = []
    for cnt in contours:
        area = cv2.contourArea(cnt)
        x_min, y_min, width, height = cv2.boundingRect(cnt)
        extent = area / (width * height)

        # filter non-rectangular objects and small objects
        if (extent > np.pi / 4) and (area > 100):
            boxes.append((x_min, y_min, x_min + width, y_min + height))

    # run over the boxes and search in each box qr-codes
    codes = []  # registers the qr-codes content and place
    for x_min, y_min, x_max, y_max in boxes:
        roi = f[y_min:y_max, x_min:x_max]

        for code in find_qr_codes(roi, symbols=[ZBarSymbol.QRCODE]):
            # get the place on the mat
            rect = list(code.rect)
            rect[0] += x_min
            rect[1] += y_min
            codes.append((code.data.decode('utf-8'), rect))  # append the content and place

    return codes


def alpha_merge(small_foreground, background, top, left):
    """
    Puts a small BGRA picture in front of a larger BGR background.
    :param small_foreground: The overlay image. Must have 4 channels.
    :param background: The background. Must have 3 channels.
    :param top: Y position where to put the overlay.
    :param left: X position where to put the overlay.
    :return: a copy of the background with the overlay added.
    """
    result = background.copy()
    # From everything I read so far, it seems we need the alpha channel separately
    # so let's split the overlay image into its individual channels
    fg_b, fg_g, fg_r, fg_a = cv2.split(small_foreground)
    # Make the range 0...1 instead of 0...255
    fg_a = fg_a / 255.0
    # Multiply the RGB channels with the alpha channel
    label_rgb = cv2.merge([fg_b * fg_a, fg_g * fg_a, fg_r * fg_a])

    # Work on a part of the background only
    height, width = small_foreground.shape[0], small_foreground.shape[1]
    part_of_bg = result[top:top + height, left:left + width, :]
    # Same procedure as before: split the individual channels
    bg_b, bg_g, bg_r = cv2.split(part_of_bg)
    # Merge them back with opposite of the alpha channel
    part_of_bg = cv2.merge([bg_b * (1 - fg_a), bg_g * (1 - fg_a), bg_r * (1 - fg_a)])

    # Add the label and the part of the background
    cv2.add(label_rgb, part_of_bg, part_of_bg)
    # Replace a part of the background
    result[top:top + height, left:left + width, :] = part_of_bg
    return result


if __name__ == '__main__':
    print('wrong program')
