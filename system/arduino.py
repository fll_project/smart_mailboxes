# functionality
import pyfirmata
from cv2 import waitKey

# system
import time
import logging
from threading import Thread

# definitions
logger = logging.getLogger()


class MailboxSystem:
    def __init__(self, board_path, door_pin, button_pin):
        # set the board
        self.board = pyfirmata.Arduino(board_path)
        pyfirmata.util.Iterator(self.board).start()
        self.write_screen('The system\nwakes up')

        self.pins = {'door': self.board.digital[door_pin],
                     'button': self.board.get_pin(f'd:{button_pin}:i')
                     }
        self.pins['door'].mode = pyfirmata.SERVO  # init the motor door pin as servo

    def open_door(self, print_log='door opened'):
        # open the door
        self.pins['door'].write(100)
        time.sleep(0.5)

        logger.info(print_log)

    def close_door(self, print_log='door closed'):
        # close the door
        self.pins['door'].write(0)
        time.sleep(0.5)

        logger.info(print_log)

    def write_screen(self, text, sec=2.0, print_log=''):
        text += '\n' if text.find('\n') == -1 else ''  # extend to 2 lines
        str1, str2 = text.split('\n')[:2]  # separate the strings

        # calc the spaces before
        spaces1 = 8 - int(len(str1) / 2)
        spaces2 = 8 - int(len(str2) / 2)
        # center the text
        str1 = f'{spaces1 * " "}{str1}'
        str2 = f'{(spaces2 - 2) * " "}{str2}'

        # write the strings to the screen
        self.board.send_sysex(pyfirmata.STRING_DATA, pyfirmata.util.str_to_two_byte_iter(str1))
        self.board.send_sysex(pyfirmata.STRING_DATA, pyfirmata.util.str_to_two_byte_iter(str2))

        if print_log:
            logger.info(print_log)
        time.sleep(sec)  # wait (for letting the user read)

    def wait_click(self, print_log=''):
        read_value = self.pins['button'].read  # set the read method
        start_value = read_value()  # read the start mode
        # wait until the button mode changes
        while read_value() == start_value:
            waitKey(1)
            time.sleep(0.001)

        if print_log:
            logger.info(print_log)


def seven_boom(sys):
    c = 1
    sys.wait_click()
    while True:
        if c % 7 == 0 or '7' in str(c):
            sys.write_screen('BOOM', sec=0)
            sys.pins['door'].write(50)
            time.sleep(0.1)
            sys.pins['door'].write(0)
        else:
            sys.write_screen(str(c), sec=0)
        c += 1
        sys.wait_click()
        time.sleep(0.2)


if __name__ == '__main__':
    system = MailboxSystem(*get_ports('ports.json'))
    seven_boom(system)
